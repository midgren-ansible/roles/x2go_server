# Role x2go_server

Ansible role to install x2go_server.

Installs the Mate desktop and Sets up an x2go server to connect to it.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

* `x2go_desktop_bindings`

    A list of desktop environments that should be supported. Possible values:

    * `lxde` - Installs support for LXDE desktop and x2go bindings
    * `mate` - Installs support for Mate desktop and x2go bindings
    * `plasma` - Requires KDE Plasma to be installed on host but enables
                 it to be accessed from x2go


## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: x2go_server
      vars:
        x2go_desktop_bindings: ['plasma']
```

## License

BSD 2-clause
